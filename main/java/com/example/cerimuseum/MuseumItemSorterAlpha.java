package com.example.cerimuseum;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MuseumItemSorterAlpha extends MuseumItemSorter {
    @Override
    public int compare(MuseumItem o1, MuseumItem o2) {
        return o1.getName().compareTo(o2.getName());
    }

    @Override
    public CharSequence getName() {
        return "Tri par ordre alphabétique";
    }
}
