package com.example.cerimuseum;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ItemAdapter extends RecyclerView.Adapter<ItemViewHolder> implements Filterable {

    public static final int CATEGORYNAME = 0;
    public static final int NORMALITEM = 1;

    private Activity activity;
    private ItemFilter filter;
    private ArrayList<MuseumItem> items;

    public ItemAdapter(Activity activity) {
        this.activity = activity;
        items = MuseumData.getItems();
        filter = new ItemFilter(this);
    }

    public void setItems(ArrayList<MuseumItem> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if(!MuseumData.getSorter().getClass().equals(MuseumItemSorterCategory.class))
        {
            return NORMALITEM;
        }

        // Calculate categories titles positions
        if(items.get(position).getId().equals(MuseumItem.UNDEFINEDID))
        {
            return CATEGORYNAME;
        }
        return NORMALITEM;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        ItemViewHolder holder;
        if(viewType == NORMALITEM) {
            View view = inflater.inflate(R.layout.item_list_element, parent, false);
            holder = new ItemViewHolder(view);
        } else {
            View view = inflater.inflate(R.layout.category_list_element, parent, false);
            holder = new ItemCategoryViewHolder(view);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        if(getItemViewType(position) == NORMALITEM) {
            holder.setItem(items.get(position));
        } else {
            int count = 0;
            for (Map.Entry<String, ArrayList<Integer>> entry : MuseumData.getCategoriesCorrespondances().entrySet()) {
                ArrayList<Integer> list = entry.getValue();
                for(int i = 0; i < list.size(); ++i) {
                    if(count == position) {
                        ((ItemCategoryViewHolder) holder).setCategory(entry.getKey());
                        return;
                    }
                    count++;
                }
                count++;
            }
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }
}
