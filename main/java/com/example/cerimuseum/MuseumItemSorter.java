package com.example.cerimuseum;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public abstract class MuseumItemSorter implements Comparator<MuseumItem> {
    public ArrayList<MuseumItem> SortLogic(ArrayList<MuseumItem> items)
    {
        Collections.sort(items, this);
        return items;
    }

    public abstract CharSequence getName();
}
