package com.example.cerimuseum;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MuseumItemActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_museum_item);

        MuseumItem item = MuseumData.getItem(getIntent().getStringExtra(MuseumItem.TAG));

        ((TextView) findViewById(R.id.itemName)).setText(item.getName());
        ((TextView) findViewById(R.id.itemBrand)).setText(item.getBrand());
        ((TextView) findViewById(R.id.itemYear)).setText(item.getYearText());
        ((TextView) findViewById(R.id.itemDemo)).setText(item.getDemoText());

        String text = "";
        for(int i = 0; i < item.getCategories().size(); ++i)
        {
            if(i != 0)
            {
                text += "\n";
            }
            text += item.getCategories().get(i);
        }
        ((TextView) findViewById(R.id.itemCategories)).setText(text);

        text = "";
        for(int i = 0; i < item.getTimeFrame().size(); ++i)
        {
            if(i != 0)
            {
                text += "\n";
            }
            text += "" + item.getTimeFrame().get(i);
        }
        ((TextView) findViewById(R.id.itemDecades)).setText(text);

        text = "";
        for(int i = 0; i < item.getTechnicalDetails().size(); ++i)
        {
            if(i != 0)
            {
                text += "\n";
            }
            text += "" + item.getTechnicalDetails().get(i);
        }
        ((TextView) findViewById(R.id.itemDetails)).setText(text);

        ((ImageView) findViewById(R.id.itemThumbnail)).setImageBitmap(item.getThumbnail());

        LinearLayout layout = (LinearLayout) (findViewById(R.id.itemPictures));
        for(int i = 0; i < item.getPictures().size(); ++i)
        {
            LinearLayout imageLayout = new LinearLayout(MuseumItemActivity.this);
            imageLayout.setOrientation(LinearLayout.VERTICAL);

            ImageView imageView = new ImageView(MuseumItemActivity.this);
            imageView.setImageBitmap(item.getPictures().get(i).bitmap);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            layoutParams.setMargins(0, 0, 0, 0);
            imageView.setLayoutParams(layoutParams);
            imageLayout.addView(imageView);

            TextView textView = new TextView(MuseumItemActivity.this);
            textView.setText(item.getPictures().get(i).description);
            textView.setTextSize(18);
            textView.setGravity(Gravity.CENTER);
            imageLayout.addView(textView);

            layout.addView(imageLayout);
        }
    }
}
