package com.example.cerimuseum;

import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.IOException;
import java.net.URL;

public class ImageDownloader implements Runnable {

    private MuseumItem item;

    public ImageDownloader(MuseumItem item) {
        this.item = item;
    }

    @Override
    public void run() {
        // Download thumbnail
        try {
            item.setThumbnail(BitmapFactory.decodeStream(new URL("https://demo-lia.univ-avignon.fr/cerimuseum/items/" + item.getId() + "/thumbnail").openStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Download pictures
        for(int i = 0; i < item.getPictures().size(); ++i) {
            MuseumItemPicture current = item.getPictures().get(i);
            try {
                current.bitmap = BitmapFactory.decodeStream(new URL("https://demo-lia.univ-avignon.fr/cerimuseum/items/" + item.getId() + "/images/" + current.link).openStream());
                Log.d("MYAPP", "Downloaded " + current.description + " of " + item.getName());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
