package com.example.cerimuseum;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class MuseumItemSorterCategory extends MuseumItemSorter {
    @Override
    public ArrayList<MuseumItem> SortLogic(ArrayList<MuseumItem> items)
    {
        HashMap<String, ArrayList<Integer>> correspondances = MuseumData.getCategoriesCorrespondances();
        ArrayList<MuseumItem> sorted = new ArrayList<MuseumItem>();

        for (Map.Entry<String, ArrayList<Integer>> entry : correspondances.entrySet())
        {
            sorted.add(new MuseumItem());
            ArrayList<Integer> list = entry.getValue();
            for(int i = 0; i < list.size(); ++i) {
                sorted.add(items.get(list.get(i)));
            }
        }
        return sorted;
}

    @Override
    public CharSequence getName() {
        return "Tri par catégories";
    }

    @Override
    public int compare(MuseumItem o1, MuseumItem o2) {
        return 0;
    }
}
