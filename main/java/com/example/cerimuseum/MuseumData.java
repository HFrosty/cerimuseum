package com.example.cerimuseum;

import android.content.Context;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class MuseumData {

    private static final String CACHENAME = "/cerimuseumdata.cache";

    private static MuseumData singleton;
    private ArrayList<MuseumItem> items;
    private MuseumItemSorter sorter;

    public MuseumData() {
        items = new ArrayList<MuseumItem>();
        sorter = new MuseumItemSorterAlpha();
    }

    public static void initialize(Context context) {
        File cache = new File(context.getCacheDir(), CACHENAME);
        singleton = new MuseumData();
    }


    public static MuseumData getInstance() {
        return singleton;
    }

    public void addItem(MuseumItem item) {
        items.add(item);
    }

    public boolean hasItemOfId(String id) {
        for(int i = 0; i < items.size(); ++i) {
            if(items.get(i).getId().equals(id)) {
                return true;
            }
        }
        return false;
    }

    public static HashMap<String, ArrayList<Integer>> getCategoriesCorrespondances() {
        HashMap<String, ArrayList<Integer>> map = new HashMap<String, ArrayList<Integer>>();

        for(int i = 0; i < singleton.items.size(); ++i) {
            MuseumItem current = singleton.items.get(i);
            for(int j = 0; j < current.getCategories().size(); ++j) {
                String cat = current.getCategories().get(j);
                if(!map.containsKey(cat))
                {
                    map.put(cat, new ArrayList<Integer>());
                }
                map.get(cat).add(i);
            }
        }
        return map;
    }

    public static void setSorter(MuseumItemSorter sorter) {
        singleton.sorter = sorter;
    };

    public static MuseumItemSorter getSorter() {
        return singleton.sorter;
    }

    public static int size() {
        return singleton.items.size();
    }

    public static MuseumItem getItem(int index) {
        return singleton.items.get(index);
    }

    public static MuseumItem getItem(String id) {
        for(int i = 0; i < singleton.items.size(); ++i) {
            if(singleton.items.get(i).getId().equals(id))
            {
                return singleton.items.get(i);
            }
        }
        return null;
    }

    public static ArrayList<MuseumItem> getItems() { return singleton.sorter.SortLogic(singleton.items); }
}
