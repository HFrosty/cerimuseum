package com.example.cerimuseum;

import android.graphics.Bitmap;

public class MuseumItemPicture {

    public String description;
    public String link;
    public Bitmap bitmap = null;
}
