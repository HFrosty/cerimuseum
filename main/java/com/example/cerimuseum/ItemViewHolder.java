package com.example.cerimuseum;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ItemViewHolder extends RecyclerView.ViewHolder {

    private int viewType;
    private MuseumItem item;

    public ItemViewHolder(@NonNull View itemView) {
        super(itemView);
        this.item = null;
        itemView.setOnClickListener((v) -> {onClick();});
    }

    public void onClick() {
        Intent intent = new Intent(itemView.getContext(), MuseumItemActivity.class);
        intent.putExtra(MuseumItem.TAG, item.getId());
        itemView.getContext().startActivity(intent);
    }

    public void setItem(MuseumItem museumItem) {
        item = museumItem;
        ((TextView) itemView.findViewById(R.id.elementCategoryTitle)).setText(museumItem.getName()); // Display name
        ((TextView) itemView.findViewById(R.id.elementYear)).setText(museumItem.getYearText()); // Display year
        ((TextView) itemView.findViewById(R.id.elementBrand)).setText(museumItem.getBrand()); // Display brand

        // Display categories
        ArrayList<String> categories = museumItem.getCategories();
        String content = "";
        for(int i = 0; i < categories.size(); ++i)
        {
            if(i != 0)
            {
                content += " - ";
            }
            content += categories.get(i);
        }
        ((TextView) itemView.findViewById(R.id.elementCategories)).setText(content); // Display brand

        if(museumItem.getThumbnail() != null) // If the item has a thumbnail, display it
        {
            ((ImageView) itemView.findViewById(R.id.elementThumbnail)).setImageBitmap(museumItem.getThumbnail());
        }
    }
}
