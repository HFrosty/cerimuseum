package com.example.cerimuseum;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class MuseumItem {

    public final static String TAG = MuseumItem.class.getName();
    public final static String UNDEFINEDID = "ID inconnu";
    public final static String UNDEFINEDNAME = "Nom inconnu";
    public final static String UNDEFINEDDESCRIPTION = "Aucune description";
    public final static String UNDEFINEDBRAND = "Marque inconnue";
    public final static int UNDEFINEDYEAR = 1337;
    public final static ArrayList<String> UNDEFINEDCATEGORIES = new ArrayList<String>() { {add("Catégorie inconnue"); }};
    public final static ArrayList<String> UNDEFINEDTECHNICALS = new ArrayList<String>() { {add("Détails techniques inconnus"); }};
    public final static ArrayList<Integer> UNDEFINEDTIMEFRAME = new ArrayList<Integer>() { {add(UNDEFINEDYEAR); add(UNDEFINEDYEAR); }};
    public final static Date UNDEFINEDDEMO = new Date(1337);

    private String id;
    private String description;
    private ArrayList<Integer> timeFrame;
    private String name;
    private ArrayList<String> categories;
    private ArrayList<String> technicalDetails;
    private int year;
    private String brand;
    private Bitmap thumbnail;
    private ArrayList<MuseumItemPicture> pictures;
    private Date nextDemo;

    public MuseumItem(String id, String description, ArrayList<Integer> timeFrame, String name, ArrayList<String> categories, ArrayList<String> technicalDetails, int year, String brand, ArrayList<MuseumItemPicture> pictures, Date nextDemo) {
        this.id = id;
        this.description = description;
        this.timeFrame = timeFrame;
        this.name = name;
        this.categories = categories;
        this.technicalDetails = technicalDetails;
        this.year = year;
        this.brand = brand;
        this.thumbnail = null;
        this.pictures = pictures;
        this.nextDemo = nextDemo;
        downloadImages();
    }

    public MuseumItem() {
        id = UNDEFINEDID;
        name = UNDEFINEDNAME;
        description = UNDEFINEDDESCRIPTION;
        timeFrame = UNDEFINEDTIMEFRAME;
        categories = UNDEFINEDCATEGORIES;
        technicalDetails = UNDEFINEDTECHNICALS;
        year = UNDEFINEDYEAR;
        brand = UNDEFINEDBRAND;
        nextDemo = UNDEFINEDDEMO;
        thumbnail = null;
        pictures = new ArrayList<MuseumItemPicture>();
    }

    public String getDemoText() {
        if(nextDemo == UNDEFINEDDEMO)
        {
            return "Aucune démo prévue";
        }
        return nextDemo.toString();
    }

    public void setDemo(Date demo) {
        nextDemo = demo;
    }

    public void downloadImages() {
        new Thread(new ImageDownloader(this)).start();
    }

    public String getName() {
        return name;
    }

    public String getBrand() { return brand;}

    public ArrayList<MuseumItemPicture> getPictures() { return pictures; };

    public ArrayList<String> getCategories() { return categories;}

    public ArrayList<String> getTechnicalDetails() { return technicalDetails; }

    public ArrayList<Integer> getTimeFrame() { return timeFrame; }

    public Bitmap getThumbnail() { return thumbnail; }

    public Integer getYear() { return year; }

    public String getYearText() {
        if(year == UNDEFINEDYEAR)
        {
            return "Année inconnue";
        }
        return "" + year;
    }

    public String getId() {
        return id;
    }

    public void setThumbnail(Bitmap thumbnail) {
        this.thumbnail = thumbnail;
    }

    public boolean contains(String value)
    {
        if(("" + year).contains(value)) {
            return true;
        }
        if(brand.toLowerCase().contains(value)) {
            return true;
        }
        if(name.toLowerCase().contains(value)) {
            return true;
        }
        if(description.toLowerCase().contains(value))
        {
            return true;
        }

        for(int i = 0; i < timeFrame.size(); ++i)
        {
            if(("" + timeFrame.get(i)).contains(value))
            {
                return true;
            }
        }

        for(int i = 0; i < categories.size(); ++i)
        {
            if(("" + categories.get(i).toLowerCase()).contains(value))
            {
                return true;
            }
        }

        for(int i = 0; i < technicalDetails.size(); ++i)
        {
            if(("" + technicalDetails.get(i).toLowerCase()).contains(value))
            {
                return true;
            }
        }

        return false;
    }
}
