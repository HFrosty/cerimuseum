package com.example.cerimuseum;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

public class ItemCategoryViewHolder extends ItemViewHolder {
    public ItemCategoryViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    public void setCategory(String category) {
        ((TextView) itemView.findViewById(R.id.elementCategoryTitle)).setText(category.toUpperCase());
    }

    @Override
    public void onClick() {
        // Does nothing
    }
}
