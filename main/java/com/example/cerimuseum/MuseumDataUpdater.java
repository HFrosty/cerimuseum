package com.example.cerimuseum;

import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Date;
import java.util.Iterator;

public class MuseumDataUpdater implements Runnable {

    private MainActivity main;

    public MuseumDataUpdater(MainActivity main) {
        this.main = main;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void updateMuseumData() {

        MuseumData data = MuseumData.getInstance();

        try {
            InputStream is = new URL("https://demo-lia.univ-avignon.fr/cerimuseum/catalog/").openStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            StringBuilder sb = new StringBuilder();
            int cp;
            while ((cp = rd.read()) != -1) {
                sb.append((char) cp);
            }
            String jsonText = sb.toString();
            JSONObject catalog = new JSONObject(jsonText);

            is = new URL("https://demo-lia.univ-avignon.fr/cerimuseum/ids/").openStream();
            rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            sb = new StringBuilder();
            while ((cp = rd.read()) != -1) {
                sb.append((char) cp);
            }
            jsonText = sb.toString();
            JSONArray ids = new JSONArray(jsonText);

            is = new URL("https://demo-lia.univ-avignon.fr/cerimuseum/demos/").openStream();
            rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            sb = new StringBuilder();
            while ((cp = rd.read()) != -1) {
                sb.append((char) cp);
            }
            jsonText = sb.toString();
            JSONObject demos = new JSONObject(jsonText);

            for (int i = 0; i < ids.length(); ++i) {
                String currentId = ids.getString(i);
                if (catalog.has(currentId) && !data.hasItemOfId(currentId)) {
                    JSONObject current = catalog.getJSONObject(currentId);
                    MuseumItem newItem = MuseumItemFactory.buildItemFromJson(currentId, current);
                    data.addItem(newItem);
                }

                main.runOnUiThread(new Runnable() {
                    public void run() {
                        main.updateDisplay();
                    }
                });
            }

            Iterator<String> keys = demos.keys();
            while(keys.hasNext()) {
                String next = keys.next();
                if(MuseumData.getItem(next) != null) {
                    TemporalAccessor ta = DateTimeFormatter.ISO_INSTANT.parse(demos.getString(next));
                    Instant i = Instant.from(ta);
                    Date d = Date.from(i);
                    MuseumData.getItem(next).setDemo(d);
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void run() {
        updateMuseumData();
    }
}
