package com.example.cerimuseum;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public abstract class MuseumItemFactory {

    public static MuseumItem buildItemFromJson(String id, JSONObject json) {
        String description = MuseumItem.UNDEFINEDDESCRIPTION;
        ArrayList<Integer> timeFrame = MuseumItem.UNDEFINEDTIMEFRAME;
        String name = MuseumItem.UNDEFINEDNAME;
        ArrayList<String> categories = MuseumItem.UNDEFINEDCATEGORIES;
        ArrayList<String> technicalDetails = MuseumItem.UNDEFINEDTECHNICALS;
        int year = MuseumItem.UNDEFINEDYEAR;
        String brand = MuseumItem.UNDEFINEDBRAND;
        Bitmap thumbnail = null;
        Date nextDemo = MuseumItem.UNDEFINEDDEMO;
        ArrayList<MuseumItemPicture> pictures = new ArrayList<>();

        try {
            if(json.has("description")) {
                description = json.getString("description");
            }
            if(json.has("timeFrame")) {
                JSONArray tmp = json.getJSONArray("timeFrame");
                timeFrame = new ArrayList<Integer>();
                for(int i = 0; i < tmp.length(); ++i) {
                    timeFrame.add(tmp.getInt(i));
                }
            }
            if(json.has("name")) {
                name = json.getString("name");
            }
            if(json.has("categories")) {
                JSONArray tmp = json.getJSONArray("categories");
                categories = new ArrayList<String>();
                for(int i = 0; i < tmp.length(); ++i) {
                    categories.add(tmp.getString(i));
                }
            }
            if(json.has("technicalDetails")) {
                JSONArray tmp = json.getJSONArray("technicalDetails");
                technicalDetails = new ArrayList<String>();
                for(int i = 0; i < tmp.length(); ++i) {
                    technicalDetails.add(tmp.getString(i));
                }
            }
            if(json.has("brand")) {
                brand = json.getString("brand");
            }
            if(json.has("year")) {
                year = json.getInt("year");
            }
            if(json.has("pictures")) {
                JSONObject tmp = json.getJSONObject("pictures");
                Iterator<String> keys = tmp.keys();
                while(keys.hasNext()) {
                    MuseumItemPicture picture = new MuseumItemPicture();
                    picture.link = keys.next();
                    picture.description = tmp.getString(picture.link);
                    picture.bitmap = null;
                    pictures.add(picture);
                }
            }
        } catch(Exception e)
        {
            return new MuseumItem(id, MuseumItem.UNDEFINEDDESCRIPTION, new ArrayList<Integer>(), MuseumItem.UNDEFINEDNAME, new ArrayList<String>(), new ArrayList<String>(), 1900, MuseumItem.UNDEFINEDBRAND, new ArrayList<MuseumItemPicture>(), MuseumItem.UNDEFINEDDEMO);
        }

        return new MuseumItem(id, description, timeFrame, name, categories, technicalDetails, year, brand, pictures, nextDemo);
    }

}
