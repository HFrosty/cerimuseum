package com.example.cerimuseum;

import android.util.Log;
import android.widget.Filter;

import java.util.ArrayList;
import java.util.List;

public class ItemFilter extends Filter {

    private ItemAdapter adapter;

    public ItemFilter(ItemAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        ArrayList<MuseumItem> filtered = new ArrayList<MuseumItem>();
        ArrayList<MuseumItem> fullList = MuseumData.getItems();
        if(constraint == null || constraint.length() == 0) {
            filtered.addAll(fullList);
        }
        else {
            String filterPattern = constraint.toString().toLowerCase().trim();
            for(int i = 0; i < fullList.size(); ++i) {
                if(fullList.get(i).contains(filterPattern)) {
                    filtered.add(fullList.get(i));
                }
            }
        }
        FilterResults results = new FilterResults();
        results.values = filtered;
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.setItems((ArrayList<MuseumItem>) results.values);
    }
}
