package com.example.cerimuseum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MuseumItemSorterChrono extends MuseumItemSorter {
    @Override
    public int compare(MuseumItem o1, MuseumItem o2) {
        return o1.getYear().compareTo(o2.getYear());
    }

    @Override
    public CharSequence getName() {
        return "Tri par ordre chronologique";
    }
}
