package com.example.cerimuseum;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;

public class MainActivity extends AppCompatActivity {

    private ItemAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.mainToolbar);
        setSupportActionBar(toolbar);

        MuseumData.initialize(this);
        new Thread(new MuseumDataUpdater(this)).start();
        updateDisplay();
    }

    public void updateDisplay() {
        RecyclerView view = (RecyclerView) findViewById(R.id.itemList);
        ItemAdapter adapter = new ItemAdapter(this);
        view.setAdapter(adapter);
        view.setLayoutManager(new LinearLayoutManager(this));
        this.adapter = adapter;
    }

    public void openSortOptions() {
        final MuseumItemSorter[] sorters = new MuseumItemSorter[]{new MuseumItemSorterAlpha(), new MuseumItemSorterChrono(), new MuseumItemSorterCategory()};
        AlertDialog.Builder builder = new  AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Choisissez une option de tri");
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(!MuseumData.getSorter().getClass().equals(sorters[which].getClass())) // Only update display if new option is not the same as before
                {
                    MuseumData.setSorter(sorters[which]);
                    updateDisplay();
                }
                dialog.dismiss();
            }
        };
        CharSequence[] array = new CharSequence[]{sorters[0].getName(), sorters[1].getName(), sorters[2].getName()};

        // Fetch current option position
        int currentOptionPosition = -1;
        for(int i = 0; i < sorters.length; ++i)
        {
            if(sorters[i].getClass().equals(MuseumData.getSorter().getClass()))
            {
                currentOptionPosition = i;
            }
        }

        builder.setSingleChoiceItems(array, currentOptionPosition, listener);
        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main_toolbar_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.searchMenuButton);
        SearchView searchView = (SearchView) (searchItem.getActionView());
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.sortMenuButton:
                openSortOptions();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
